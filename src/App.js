import React, { Component } from 'react';
import { Provider } from 'react-redux';
import Orientation from 'react-native-orientation';
import { ThemeProvider } from 'styled-components';
import SplashScreen from 'react-native-splash-screen';
import { PersistGate } from 'redux-persist/es/integration/react';
import configureStore from './configureStore';
import { View, StatusBar } from './components/common';
import AppNavigator from './AppNavigator';
import theme from './styles';
import NavigationService from './NavigationService';

const { store, persistor } = configureStore();

export default class App extends Component {
  props: {
    rootSiblings: Array<any>,
  };

  componentDidMount() {
    // do stuff while splash screen is shown
    // After having done stuff (such as async tasks) hide the splash screen
    SplashScreen.hide();
  }

  componentWillMount() {
    Orientation.lockToPortrait();
  }

  render() {
    const { rootSiblings } = this.props;
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <PersistGate loading={null} persistor={persistor}>
            <View flex={1}>
              <StatusBar />
              <AppNavigator
                ref={navigatorRef => {
                  NavigationService.setTopLevelNavigator(navigatorRef);
                }}
              />
              {rootSiblings}
            </View>
          </PersistGate>
        </ThemeProvider>
      </Provider>
    );
  }
}
