import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import navReducer from './navigation';
import example from '../screens/example';
// import localstorage from '../containers/localstorage';

const appReducer = combineReducers({
  form: formReducer,
  nav: navReducer,
  [example.ducks.NAME]: example.reducer,
  //   [localstorage.ducks.NAME]: localstorage.reducer,
});

export default (state, action) =>
  //   if (action.type === GLOBAL_LOGOUT_SUCCESS) {
  //     state = {
  //     };
  //   }
  appReducer(state, action);
