// import { StackActions, NavigationActions } from 'react-navigation';
import { createNavigationReducer } from 'react-navigation-redux-helpers';
import AppNavigator from '../AppNavigator';

export const NAME = 'nav';

const navReducer = createNavigationReducer(AppNavigator);

export default navReducer;

// const initialNavState = {
//   index: 0,
//   routes: [
//     {
//       key: 'ComponentScreen',
//       routeName: 'ComponentScreen',
//     },
//   ],
// };

// export default (state = initialNavState, action) => {
//   if (
//     action.type === StackActions.PUSH ||
//     action.type === NavigationActions.NAVIGATE
//   ) {
//     const { routes, index } = state;
//     const { routeName } = action;
//     const currentScreen = routes[index];
//     if (!currentScreen.routes && currentScreen.routeName === routeName) {
//       return state;
//     }
//   }
//   return AppNavigator.router.getStateForAction(action, state);
// };
