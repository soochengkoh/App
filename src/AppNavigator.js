import { createStackNavigator } from 'react-navigation';
import ComponentScreen from './screens/example/ComponentScreen';
import RequestScreen from './screens/example/request/RequestScreen';
import WebViewScreen from './screens/webview/WebViewScreen';
import theme from './styles';

export const defaultNavigationOptions = {
  gesturesEnabled: true,
  headerStyle: theme.headerStyle,
  headerTintColor: theme.headerTintColor,
  headerBackTitle: null,
  headerTitleStyle: theme.headerTitleStyle,
};

export const routeConfigs = {
  ComponentScreen: { screen: ComponentScreen },
  RequestScreen: { screen: RequestScreen },
  WebViewScreen: { screen: WebViewScreen },
};

const AppNavigator = createStackNavigator(routeConfigs, {
  navigationOptions: ({ navigationOptions }) => ({
    ...defaultNavigationOptions,
    ...navigationOptions,
  }),
  initialRouteName: 'ComponentScreen',
});

export default AppNavigator;
