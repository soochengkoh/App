import Config from 'react-native-config';

let config;
if (Config.ENVIRONMENT === 'production' || Config.ENVIRONMENT === 'staging') {
  config = require('./config.production').default;
} else {
  config = require('./config.development').default;
}

export default {
  ...config,
};
