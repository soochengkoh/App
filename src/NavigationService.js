import { NavigationActions, StackActions } from 'react-navigation';

let _navigator;

const setTopLevelNavigator = navigatorRef => {
  _navigator = navigatorRef;
};

const navigateActions = ['navigate'];

const navigate = navigateActions.reduce((obj, action) => {
  obj[action] = () =>
    _navigator.dispatch(NavigationActions[action].apply(null, arguments));
  return obj;
}, {});

const stackActions = ['pop', 'push', 'popToTop', 'reset', 'replace'];
const stack = stackActions.reduce((obj, action) => {
  obj[action] = (...args) =>
    _navigator.dispatch(StackActions[action].apply(null, args));
  return obj;
}, {});

export default {
  setTopLevelNavigator,
  ...navigate,
  ...stack,
};
