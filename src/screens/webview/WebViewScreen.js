import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import get from 'lodash/get';
import { View, HeaderButton } from '../../components/common';
import WebView, { refs, WEBVIEW_REF } from '../../components/common/WebView';

class WebViewScreen extends Component {
  props: {
    navigation: Object,
  };

  state = {};

  onNavigationStateChange = navState => {
    const { navigation } = this.props;
    if (navigation.state.params.backButtonEnabled !== navState.canGoBack) {
      navigation.setParams({
        backButtonEnabled: navState.canGoBack,
      });
    }
    if (get(navState, 'title')) {
      navigation.setParams({
        htmlTitle: get(navState, 'title'),
      });
    }
  };

  render() {
    const { navigation } = this.props;
    const { uri } = navigation.state.params;
    return (
      <WebView
        uri={uri}
        onNavigationStateChange={this.onNavigationStateChange}
        onLoadStart={this.handleOnLoad}
        onLoad={this.handleOnLoad}
        onError={this.handleOnLoad}
      />
    );
  }
}

WebViewScreen.navigationOptions = ({ navigation }) => ({
  title:
    get(navigation, 'state.params.webViewTitle') ||
    get(navigation, 'state.params.htmlTitle') ||
    '',
  headerLeft: (
    <View>
      {!!get(navigation, 'state.params.backButtonEnabled') && (
        <HeaderButton
          onPress={() => {
            const webView = refs[WEBVIEW_REF];
            if (webView && webView.goBack) {
              webView.goBack();
            }
          }}
          icon="back"
          color="white"
        />
      )}
    </View>
  ),
  headerRight: (
    <HeaderButton
      onPress={() => {
        navigation.dispatch(NavigationActions.back());
      }}
      icon="cross"
      color="white"
    />
  ),
  gesturesEnabled: false,
});

export default WebViewScreen;
