import { combineReducers } from 'redux';
import {
  createRequestEpicDucks,
  createNamePrefix,
} from 'redux-observable-utils';
import { NAME as PARENT_NAME } from '../constants';
import * as api from './api';

export const NAME = 'REQUEST';

export const PREFIXED_NAME = createNamePrefix(NAME, PARENT_NAME);

export const { ducks: todos, epic: todosEpic } = createRequestEpicDucks({
  moduleName: PREFIXED_NAME,
  reducerName: 'TODOS',
  api: api.findTodos,
});

export default combineReducers({
  [todos.reducerName]: todos.reducer,
});
