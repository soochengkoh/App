export const findTodos = () => new Promise(resolve => {
  setTimeout(() => {
    resolve(['Do homework', 'Laundry', 'Prepare speech']);
  }, 1500);
});
