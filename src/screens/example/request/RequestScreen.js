import React, { Component } from 'react';
import { connect } from 'react-redux';
import get from 'lodash/get';
import { Spinner, View, ListItem } from '../../../components/common';
import * as ducks from './ducks';

type Props = {
  todosFetch: Function,
  todos: Object,
};

class RequestScreen extends Component {
  props: Props;

  componentDidMount() {
    const { todosFetch } = this.props;
    todosFetch();
  }

  render() {
    const { todos } = this.props;
    return (
      <View>
        <Spinner overlay visible={todos.isFetching} />
        {!!get(todos, 'payload.length') &&
          todos.payload.map(todo => <ListItem key={todo} label={todo} />)}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  todos: ducks.todos.selector(state),
});

const mapDispatchToProps = {
  todosFetch: ducks.todos.requestActions.fetch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RequestScreen);
