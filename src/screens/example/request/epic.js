import { combineEpics } from 'redux-observable';
import { todosEpic } from './ducks';

export default combineEpics(todosEpic);
