import { combineEpics } from 'redux-observable';
import request from './request';

export default combineEpics(request.epic);
