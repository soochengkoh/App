import { combineReducers } from 'redux';
import request from './request';

export { NAME } from './constants';

export default combineReducers({
  [request.ducks.NAME]: request.reducer,
});
