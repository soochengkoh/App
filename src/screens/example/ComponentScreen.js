import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components/native';
import ReactTimeout from 'react-timeout';
import {
  Banner,
  Button,
  Header,
  HeaderIcon,
  Radio,
  ScrollView,
  Spacing,
  Modal,
  Spinner,
  Text,
  Icon,
  Touchable,
  Divider,
  ListItem,
  SectionHeader,
  Container,
  Checkbox,
  View,
  Select,
  Dialog,
  IconButton,
  SegmentedControl,
} from '../../components/common';
import toast from '../../utils/toast';

const Content = styled.View`
  padding: 15px;
`;

class ComponentScreen extends Component {
  props: {
    setTimeout: Function,
    navigation: Object,
  };

  static navigationOptions = () => ({
    title: 'Component',
  });

  state = {
    dialogVisible: false,
    checkbox: true,
    overlaySpinnerVisible: false,
    modalVisible: false,
    segmentedIndex: 0,
    segmentedValues: ['By Yield', 'By Performance'],
  };

  render() {
    const { setTimeout, navigation } = this.props;
    return (
      <Container backgroundColor="white">
        <ScrollView>
          <SectionHeader title="SegmentedControl" />
          <Content>
            <SegmentedControl
              values={this.state.segmentedValues}
              selectedIndex={this.state.segmentedIndex}
              onValueChange={value =>
                this.setState({
                  segmentedIndex: value,
                })
              }
            />
          </Content>

          <SectionHeader title="Banner" />
          <Content>
            <Banner
              items={[
                {
                  imageSource:
                    'https://timedotcom.files.wordpress.com/2015/09/googles-new-logo-5078286822539264-2-hp.gif',
                },
                {
                  imageSource:
                    'https://timedotcom.files.wordpress.com/2015/09/googles-new-logo-5078286822539264-2-hp.gif',
                },
                {
                  imageSource:
                    'https://timedotcom.files.wordpress.com/2015/09/googles-new-logo-5078286822539264-2-hp.gif',
                },
              ]}
            />
          </Content>
          <SectionHeader title="Dialog" />
          <Content>
            <Button
              primary
              onPress={() =>
                this.setState({
                  dialogVisible: true,
                })
              }
            >
              Open Dialog
            </Button>
            <Dialog
              visible={this.state.dialogVisible}
              onClose={() => this.setState({ dialogVisible: false })}
              title="Dialog"
              actions={[
                {
                  label: 'Cancel',
                  onPress: () => this.setState({ dialogVisible: false }),
                  backgroundColor: 'grey',
                },
                {
                  label: 'Ok',
                  onPress: () => toast.show('OK'),
                  primary: true,
                },
              ]}
            >
              <View padding={20} backgroundColor="transparent">
                <Text>Do you want to save?</Text>
              </View>
            </Dialog>
          </Content>

          <SectionHeader title="Select" />
          <Content>
            <Select
              title="Country"
              label="Country"
              value={this.state.country}
              onChange={value => this.setState({ country: value })}
              placeholder="Select Country"
              items={[
                {
                  label: 'Malaysia',
                  value: 'MY',
                },
                {
                  label: 'Singapore',
                  value: 'SG',
                },
                {
                  label: 'Hong Kong',
                  value: 'HK',
                },
              ]}
            />
          </Content>

          <SectionHeader title="Button" />
          <Content>
            <Button primary borderRounded onPress={() => {}}>
              Primary Button
            </Button>
          </Content>
          <Content>
            <IconButton primary name="refresh" onPress={() => {}} />
          </Content>
          <Content>
            <Button secondary borderRounded onPress={() => {}}>
              Secondary Button
            </Button>
          </Content>
          <Content>
            <Button primary borderRounded block onPress={() => {}}>
              Block Button
            </Button>
          </Content>
          <Content>
            <Button primary disabled borderRounded onPress={() => {}}>
              Disabled Button
            </Button>
          </Content>
          <SectionHeader title="Checkbox" />
          <Content>
            <Checkbox
              label="Checkbox"
              value={this.state.checkbox}
              onChange={value => {
                this.setState({ checkbox: value });
              }}
            />
          </Content>
          <Content>
            <Checkbox
              label="Disabled"
              value={false}
              onChange={() => {}}
              disabled
            />
          </Content>
          <Content>
            <Checkbox
              label="Non Editable"
              value
              onChange={() => {}}
              editable={false}
            />
          </Content>
          <SectionHeader title="Header" />
          <Content>
            <Header
              title="Header"
              leftComponent={() => <HeaderIcon name="refresh" color="white" />}
              rightComponent={() => <HeaderIcon name="refresh" color="white" />}
            />
          </Content>
          <SectionHeader title="Radio" />
          <Content>
            <Radio
              label="A"
              value={this.state.radio}
              radioValue="A"
              onChange={value => {
                this.setState({ radio: value });
              }}
            />
          </Content>
          <Content>
            <Radio
              label="B"
              value={this.state.radio}
              radioValue="B"
              onChange={value => {
                this.setState({ radio: value });
              }}
            />
          </Content>
          <SectionHeader title="Spinner" />
          <Content>
            <Spinner visible color="primary" />
          </Content>
          <Content>
            <Button
              backgroundColor="primary"
              color="light.text.primary"
              borderRounded
              onPress={() => {
                this.setState({
                  overlaySpinnerVisible: true,
                });
                setTimeout(() => {
                  this.setState({
                    overlaySpinnerVisible: false,
                  });
                }, 2000);
              }}
            >
              Show overlay spinner
            </Button>
            <Spinner
              visible={this.state.overlaySpinnerVisible}
              overlay
              color="primary"
            />
          </Content>
          <SectionHeader title="Spacing" />
          <Content>
            <Spacing height={25} backgroundColor="blue" />
            <Spacing height={50} width={50} backgroundColor="secondary" />
          </Content>
          <SectionHeader title="Text" />
          <Content>
            <Text color="primary">Primary Text</Text>
            <Text color="secondary">Secondary Text</Text>
            <Text color="dark.text.primary">Dark Primary Text</Text>
            <Text color="dark.text.secondary">Dark Secondary Text</Text>
            <Text color="dark.text.hint">Dark Hint Text</Text>
          </Content>

          <SectionHeader title="Icon" />
          <Content>
            <Icon name="refresh" />
            <Icon name="refresh" size={50} />
            <Icon name="refresh" size={100} color="secondary" />
          </Content>

          <SectionHeader title="Divider" />
          <Content>
            <Divider />
          </Content>
          <Content>
            <Divider large />
          </Content>

          <SectionHeader title="ListItem" />
          <Content>
            <ListItem label="Apple" leftIcon="refresh" />
            <ListItem label="Boy" rightIcon="refresh" />
            <ListItem label="Cat" leftIcon="refresh" rightIcon="refresh" />
            <ListItem label="Dog" />
            <ListItem label="Elephant" suffix="Flower" />
          </Content>

          <SectionHeader title="Modal" />
          <Content>
            <Button
              backgroundColor="primary"
              color="light.text.primary"
              borderRounded
              onPress={() => {
                this.setState({
                  modalVisible: true,
                });
              }}
            >
              Show modal
            </Button>
            <Modal
              visible={this.state.modalVisible}
              onClose={() => this.setState({ modalVisible: false })}
            >
              <View backgroundColor="white" padding={25}>
                <Text>Modal content here</Text>
              </View>
            </Modal>
          </Content>
          <SectionHeader title="Touchable, Toast" />
          <Content>
            <Touchable onPress={() => toast.show('touchable pressed')}>
              <Text>Show Toast on Touchable pressed</Text>
            </Touchable>
          </Content>

          <SectionHeader title="WebViewScreen" />
          <Content>
            <Button
              borderRounded
              color="light.text.primary"
              backgroundColor="primary"
              onPress={() =>
                navigation.push('WebViewScreen', {
                  uri: 'https://www.google.com',
                })
              }
            >
              WebViewScreen
            </Button>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = () => ({
  //   country: getCountry(state),
});

ComponentScreen = ReactTimeout(ComponentScreen);

export default connect(mapStateToProps)(ComponentScreen);
