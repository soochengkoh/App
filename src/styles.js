import DeviceInfo from 'react-native-device-info';
import { Platform } from 'react-native';
import { TabBarBottom } from 'react-navigation';

export const isIPhoneX = DeviceInfo.getModel() === 'iPhone X';

export const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const IOS_STATUSBAR_HEIGHT = isIPhoneX ? 44 : 20;
export const STATUSBAR_HEIGHT =
  Platform.OS === 'ios' ? IOS_STATUSBAR_HEIGHT : 0;
export const HEADER_HEIGHT = APPBAR_HEIGHT + STATUSBAR_HEIGHT;

const size = {
  text: {
    xxxxxxs: 9,
    xxxxxs: 10,
    xxxxs: 11,
    xxxs: 12,
    xxs: 13,
    xs: 14,
    s: 15,
    m: 16,
    l: 17,
    xl: 18,
    xxl: 19,
    xxxl: 20,
    xxxxl: 21,
    xxxxxl: 22,
    xxxxxxl: 23,
  },
  icon: {
    xxxxxxs: 9,
    xxxxxs: 10,
    xxxxs: 11,
    xxxs: 12,
    xxs: 13,
    xs: 14,
    s: 15,
    m: 16,
    l: 17,
    xl: 18,
    xxl: 19,
    xxxl: 20,
    xxxxl: 21,
    xxxxxl: 22,
    xxxxxxl: 23,
  },
};

const color = {
  primary: '#F44336', // 500
  primary100: '#FFCDD2',
  primary300: '#E57373',
  primary700: '#D32F2F',
  secondary: '#536DFE', // A200
  secondaryA100: '#8C9EFF',
  secondaryA400: '#3D5AFE',
  secondaryA700: '#304FFE',
  white: '#FFF',
  disabledBackgroundColor: '#00000061',
  disabledTextColor: '#FFFFFF80',
  grey: '#E0E0E0',
  grey100: '#F5F5F5',
  grey50: '#FAFAFA',
  profit: '#66BB6A',
  loss: '#F44336',
  dark: {
    text: {
      primary: '#000000DE',
      secondary: '#0000008A',
      hint: '#00000061',
      disabled: '#00000061',
    },
    divider: '#0000001F',
  },
  light: {
    text: {
      primary: '#FFFFFFFF',
      secondary: '#FFFFFFB3',
      hint: '#FFFFFF80',
      disabled: '#FFFFFF80',
    },
    divider: '#FFFFFF1F',
  },
  background: '#F2F2F2',
};

const headerTitleStyle = {
  fontSize: size.text.l,
  color: color.light.text.primary,
  marginHorizontal: 30,
};

export const getTabOptions = initialRouteName => ({
  initialRouteName,
  tabBarComponent: TabBarBottom,
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  animationEnabled: false,
  lazyLoad: true,
  tabBarOptions: {
    showIcon: true,
    activeTintColor: color.primary,
    inactiveTintColor: color.dark.text.hint,
    labelStyle: { fontSize: 10, margin: 0 },
    style: {
      backgroundColor: 'white',
    },
    indicatorStyle: {
      backgroundColor: 'transparent',
    },
  },
});

export default {
  footerHeight: isIPhoneX ? 44 + 34 : 44,
  footerPaddingBottom: isIPhoneX ? 34 : 0,
  appBarHeight: APPBAR_HEIGHT,
  statusBarHeight: STATUSBAR_HEIGHT,
  headerHeight: APPBAR_HEIGHT + STATUSBAR_HEIGHT,
  headerTitleStyle,
  headerTintColor: color.light.text.primary,
  headerStyle: {
    backgroundColor: color.primary,
  },
  color,
  size,
};
