import React from 'react';
import DeviceInfo from 'react-native-device-info';
import styled from 'styled-components/native';
import { isStyledThemeBy } from '../../utils/styles';

type Props = {
  backgroundColor?: string,
};

const isIPhoneX = DeviceInfo.getModel() === 'iPhone X';

// ${props =>
//     isStyledThemeBy('backgroundColor', {
//       cssProp: 'background-color',
//       at: 'color',
//       defaultValue: props.theme.color.white,
//     })(props)};

const View = styled.View`
  height: ${({ theme }) => theme.footerHeight};
  ${isStyledThemeBy('backgroundColor', {
    cssProp: 'background-color',
    at: 'color',
    defaultValue: props => props.theme.color.white,
  })};
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  padding-bottom: ${isIPhoneX ? 34 : 0};
`;

const Footer = (props: Props) => <View {...props} />;

export default Footer;
