import React from 'react';
import styled from 'styled-components/native';
import { isStyledBy, isStyledThemeBy } from '../../utils/styles';

const defaultProps = {
  height: 15,
};

const StyledView = styled.View.attrs({
  width: props => props.width || '100%',
})`
  ${isStyledBy('height')};
  ${isStyledBy('width')};
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
    defaultValue: 'transparent',
  })};
`;

type Props = {
  height?: number,
  width?: number,
};

const Spacing = (props: Props) => <StyledView {...props} />;

Spacing.defaultProps = defaultProps;

export default Spacing;
