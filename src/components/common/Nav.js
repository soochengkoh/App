import React from 'react';
import { connect } from 'react-redux';
import { StackActions } from 'react-navigation';
import View from './View';
import IconButton from './IconButton';
import Spacing from './Spacing';

const ICON_SIZE = 55;

type Props = {
  push: Function,
  navRows: Array<Array<Object>>,
  iconSize?: number,
};

const Nav = ({ push, navRows, iconSize }: Props) => (
  <View padding={15}>
    {navRows.map((navRow, index) => (
      <View key={index}>
        <View flexDirection="row" justifyContent="space-around">
          {navRow.map(nav => (
            <IconButton
              key={nav.label}
              name={nav.icon}
              backgroundColor={nav.color}
              color={null}
              size={iconSize || ICON_SIZE}
              iconSize={iconSize || ICON_SIZE}
              label={nav.label}
              onPress={() =>
                push({ routeName: nav.routeName, params: nav.params })
              }
            />
          ))}
        </View>
        {index !== navRows.length - 1 && <Spacing height={15} />}
      </View>
    ))}
  </View>
);

export default connect(
  null,
  {
    push: StackActions.push,
  },
)(Nav);
