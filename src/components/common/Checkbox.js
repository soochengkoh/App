import React, { Component } from 'react';
import styled from 'styled-components/native';
import { withTheme } from 'styled-components';
import View from './View';
import Touchable from './Touchable';
import Icon from './Icon';
import Text from './Text';

type CheckboxProps = {
  label?: any,
  labelProps?: Object,
  value: any,
  onChange: Function,
  positiveValue?: any,
  negativeValue?: any,
  disabled?: boolean,
  editable?: boolean,
  theme: Object,
};

const getCheckboxIconColor = props => {
  if (props.checked) {
    return props.theme.color.primary;
  }
  if (props.disabled) {
    return props.theme.color.dark.text.disabled;
  }
  return props.theme.color.dark.text.primary;
};

const CheckboxIcon = styled(Icon).attrs({
  size: 16,
  name: props => (props.checked ? 'checkbox_checked' : 'checkbox_unchecked'),
  color: props => getCheckboxIconColor(props),
})`
  margin-right: 8px;
`;

const Label = ({
  label,
  labelProps,
  ...rest
}: {
  label?: any,
  labelProps?: Object,
}) => {
  if (!label) {
    return null;
  }
  if (typeof label === 'string') {
    return (
      <Text {...rest} {...labelProps}>
        {label}
      </Text>
    );
  }
  return label;
};

const CheckboxContainer = styled(View)`
  flex-direction: row;
  align-items: center;
`;

class Checkbox extends Component {
  props: CheckboxProps;

  static defaultProps = {
    label: undefined,
    value: false,
    editable: true,
    disabled: false,
    onChange: () => {},
    positiveValue: true,
    negativeValue: false,
    color: undefined,
    labelProps: {},
  };

  onCheckboxChange = () => {
    const {
      value, onChange, positiveValue, negativeValue,
    } = this.props;
    onChange(value === positiveValue ? negativeValue : positiveValue);
  };

  isCheck = () => {
    const { value, positiveValue } = this.props;
    return value === positiveValue;
  };

  render() {
    const {
      label,
      labelProps,
      disabled,
      onChange,
      editable,
      theme,
    } = this.props;
    const content = (
      <CheckboxContainer>
        <CheckboxIcon
          checked={this.isCheck()}
          disabled={disabled}
          editable={editable}
        />
        <Label
          label={label}
          labelProps={labelProps}
          color={disabled ? theme.color.dark.text.disabled : undefined}
        />
      </CheckboxContainer>
    );

    if (!disabled && editable && onChange) {
      return <Touchable onPress={this.onCheckboxChange}>{content}</Touchable>;
    }
    return content;
  }
}

export default withTheme(Checkbox);
