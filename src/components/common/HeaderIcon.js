import React from 'react';
import Icon from './Icon';

export default props => <Icon size={18} {...props} />;
