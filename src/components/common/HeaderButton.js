import React from 'react';
import styled from 'styled-components';
import Touchable from './Touchable';
import HeaderIcon from './HeaderIcon';
import Text from './Text';
import { isStyledThemeBy } from '../../utils/styles';

const StyledTouchable = styled(Touchable)`
  padding-horizontal: 10px;
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
  })};
`;

type Props = {
  onPress: Function,
  icon?: string,
  label?: string,
  color?: string,
  backgroundColor?: string,
};

const HeaderButton = ({
  onPress, icon, label, color,
}: Props) => {
  let content = null;
  if (icon) {
    content = <HeaderIcon name={icon} color={color} />;
  } else if (label) {
    content = <Text color={color}>{label}</Text>;
  }
  return <StyledTouchable onPress={onPress}>{content}</StyledTouchable>;
};

export default HeaderButton;
