import React, { Component } from 'react';
import { BackHandler, WebView as RNWebView } from 'react-native';
import Spinner from './Spinner';

type Props = {
  uri: string,
  onNavigationStateChange?: Function,
};

export const WEBVIEW_REF = 'WEBVIEW_REF';
export const refs = {};

class WebView extends Component {
  props: Props;

  state = {
    backButtonEnabled: false,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    refs[WEBVIEW_REF] = undefined;
  }

  backHandler = () => {
    if (this.state.backButtonEnabled) {
      const webView = refs[WEBVIEW_REF];
      if (webView) {
        webView.goBack();
      }
      return true;
    }
  };

  onNavigationStateChange = navState => {
    const { onNavigationStateChange } = this.props;
    if (onNavigationStateChange) {
      onNavigationStateChange(navState);
    }
    this.setState({
      backButtonEnabled: navState.canGoBack,
    });
  };

  render() {
    const { uri } = this.props;
    const webViewProps = {
      ...this.props,
      ref: webView => {
        refs[WEBVIEW_REF] = webView;
      },
      source: { uri },
      onNavigationStateChange: this.onNavigationStateChange,
      domStorageEnabled: true,
      startInLoadingState: true,
      renderLoading: () => <Spinner center visible />,
      scalesPageToFit: true,
      hideKeyboardAccessoryView: true,
      javaScriptEnabled: true,
    };
    return <RNWebView {...webViewProps} />;
  }
}

export default WebView;
