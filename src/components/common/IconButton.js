import React from 'react';
import styled from 'styled-components';
import Touchable from './Touchable';
import Icon from './Icon';
import View from './View';
import Text from './Text';
import Spacing from './Spacing';
import { isStyledThemeBy, isStyledBy, is } from '../../utils/styles';

const defaultButtonSize = 50;

const getIconSize = buttonSize =>
  (buttonSize ? buttonSize / 2 : defaultButtonSize / 2);

const StyledTouchable = styled(Touchable)`
  padding-horizontal: 10px;
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
  })};
  border-radius: ${props => getIconSize(props.size)};
  ${isStyledBy('size', {
    cssProp: 'height',
    defaultValue: defaultButtonSize,
  })};
  ${isStyledBy('size', {
    cssProp: 'width',
    defaultValue: defaultButtonSize,
  })};
  align-items: center;
  justify-content: center;
  ${is('primary')`
      background-color: ${props => props.theme.color.primary};
    `};
`;

type Props = {
  onPress: Function,
  name?: string,
  color?: string,
  size?: number,
  iconSize?: number,
  backgroundColor?: string,
  label?: string,
};

const IconButton = ({
  label, name, color, size, iconSize, ...rest
}: Props) => (
  <View>
    <StyledTouchable size={size} {...rest}>
      <Icon
        name={name}
        color={color !== undefined ? color : 'white'}
        size={iconSize || getIconSize(size)}
      />
    </StyledTouchable>
    {!!label && (
      <View>
        <Spacing height={5} />
        <Text width={size} size="xxxxxs" textAlign="center">
          {label}
        </Text>
      </View>
    )}
  </View>
);

export default IconButton;
