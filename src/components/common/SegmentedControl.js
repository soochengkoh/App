import React from 'react';
import styled from 'styled-components';
import Touchable from './Touchable';
import View from './View';
import Text from './Text';

const CONTROL_WIDTH = 150;

const Control = styled(View)`
  padding-horizontal: 15px;
  padding-vertical: 8px;
  align-items: center;
  justify-content: center;
`;

type Props = {
  values: Array<any>,
  selectedIndex: number,
  onValueChange: Function,
  controlWidth?: number,
};

const SegmentedControl = ({
  values,
  selectedIndex,
  onValueChange,
  controlWidth,
}: Props) => (
  <View
    flexDirection="row"
    alignItems="center"
    justifyContent="center"
    borderColor="primary"
    borderWidth={1}
    borderRadius={15}
    alignSelf="center"
    overflow="hidden"
  >
    {values.map((value, index) => (
      <Touchable key={value} onPress={() => onValueChange(index)}>
        <Control
          width={controlWidth || CONTROL_WIDTH}
          backgroundColor={selectedIndex === index ? 'primary' : undefined}
        >
          <Text color={selectedIndex === index ? 'white' : 'primary'}>
            {value}
          </Text>
        </Control>
      </Touchable>
    ))}
  </View>
);

export default SegmentedControl;
