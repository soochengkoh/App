import React from 'react';
import styled from 'styled-components/native';
import View from './View';

const StyledView = styled(View)`
  flex: 1;
`;

const Container = props => <StyledView {...props} />;

export default Container;
