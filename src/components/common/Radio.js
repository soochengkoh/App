import React from 'react';
import styled from 'styled-components/native';
import View from './View';
import Icon from './Icon';
import Text from './Text';
import Touchable from './Touchable';

type RadioProps = {
  label?: string,
  radioValue: any,
  value?: any,
  onChange?: Function,
  disabled?: boolean,
  editable?: boolean,
};

const getRadioIconColor = props => {
  if (props.checked) {
    return props.theme.color.primary;
  }
  if (props.disabled) {
    return props.theme.color.dark.text.hint;
  }
  return props.theme.color.dark.text.primary;
};

const RadioIcon = styled(Icon).attrs({
  size: 16,
  name: props => (props.checked ? 'radio_checked' : 'radio_unchecked'),
  color: props => getRadioIconColor(props),
})`
  margin-right: 8px;
`;

const RadioContainer = styled(View)`
  flex-direction: row;
  align-items: center;
`;

const Radio = ({
  value,
  radioValue,
  label,
  disabled,
  editable,
  onChange,
}: RadioProps) => {
  const isCheck = value === radioValue;
  const content = (
    <RadioContainer>
      <RadioIcon checked={isCheck} disabled={disabled} />
      <Text>{label}</Text>
    </RadioContainer>
  );

  if (!disabled && editable && onChange) {
    return (
      <Touchable onPress={() => onChange(radioValue)}>{content}</Touchable>
    );
  }
  return content;
};

Radio.defaultProps = {
  disabled: false,
  editable: true,
};

export default Radio;
