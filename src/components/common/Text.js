import React from 'react';
import styled from 'styled-components/native';
import { isStyledBy, isStyledThemeBy } from '../../utils/styles';

type Props = {
  weight?: string,
  color?: string | boolean,
  size?: string,
  fontFamily?: string,
};

export const StyledText = styled.Text`
  ${isStyledThemeBy('color', {
    at: 'color',
    defaultValue: props => props.theme.color.dark.text.primary,
  })};
  ${isStyledThemeBy('size', {
    cssProp: 'font-size',
    at: 'size.text',
    defaultValue: props => props.theme.size.text.m,
  })};
  ${isStyledBy('weight', { cssProp: 'font-weight' })};
  ${isStyledBy('flex')};
  ${isStyledBy('textAlign')};
  ${isStyledBy('width')};
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
  })};
  ${isStyledBy('lineHeight', { cssProp: 'line-height' })};
`;

const Text = (props: Props) => <StyledText {...props} />;

export default Text;
