import React from 'react';
import { MaterialIndicator, WaveIndicator } from 'react-native-indicators';
import styled from 'styled-components/native';
import { withTheme } from 'styled-components';
import Modal from 'react-native-root-modal';
import get from 'lodash/get';
import View from './View';

type Props = {
  visible?: boolean,
  overlay?: boolean,
  size?: string,
  color?: string,
  theme: Object,
  center?: boolean,
};

const CenteredView = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  padding: 8px;
  justify-content: center;
  align-items: center;
`;

const HorizontalCenteredView = styled.View`
  align-items: center;
  justify-content: center;
  padding: 8px;
`;

const Spinner = ({
  overlay, size, visible, color, theme, center,
}: Props) => {
  const spinnerColor = get(theme.color, color) || color || theme.color.primary;
  if (overlay) {
    return (
      <Modal visible={visible}>
        <CenteredView>
          <View>
            <WaveIndicator color={spinnerColor} size={size} />
          </View>
        </CenteredView>
      </Modal>
    );
  }
  if (visible) {
    const Container = center ? CenteredView : HorizontalCenteredView;
    return (
      <Container>
        <MaterialIndicator color={spinnerColor} size={size} />
      </Container>
    );
  }
  return null;
};

export default withTheme(Spinner);
