import React from 'react';
import { withTheme } from 'styled-components';
import { StatusBar } from 'react-native';

const CustomStatusBar = ({ theme }: { theme: Object }) => (
  <StatusBar backgroundColor={theme.color.primary} barStyle="light-content" />
);

export default withTheme(CustomStatusBar);
