import React from 'react';
import styled from 'styled-components/native';
import { withTheme } from 'styled-components';
import View from './View';
import Text from './Text';
import { isStyledThemeBy } from '../../utils/styles';

const Container = styled(View)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 10px;
  padding-horizontal: 15px;
  ${props =>
    isStyledThemeBy('backgroundColor', {
      cssProp: 'background-color',
      at: 'color',
      defaultValue: props.theme.color.grey,
    })(props)};
`;

type Props = {
  title: string,
  color?: string,
  theme: Object,
};

const SectionHeader = ({
  title, color, theme, ...rest
}: Props) => (
  <Container {...rest}>
    <Text color={theme.color.dark.text.primary || color}>{title}</Text>
  </Container>
);

export default withTheme(SectionHeader);
