import React, { Component } from 'react';
import { InteractionManager } from 'react-native';
import hoistNonReactStatic from 'hoist-non-react-statics';
import Spinner from './Spinner';

const interactionDelayRender = ComponentToWrap => {
  class InteractionDelayRender extends Component {
    state = {
      renderPlaceholder: true,
    };

    componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
        this.setState({ renderPlaceholder: false });
      });
    }

    render() {
      if (this.state.renderPlaceholder) {
        return <Spinner visible center />;
      }
      return <ComponentToWrap {...this.props} />;
    }
  }

  const hoistedInteractionDelayRender = hoistNonReactStatic(
    InteractionDelayRender,
    ComponentToWrap,
  );
  return hoistedInteractionDelayRender;
};

export default interactionDelayRender;
