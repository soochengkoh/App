import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import styled, { withTheme } from 'styled-components';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import View from './View';
import Touchable from './Touchable';

type BannerItemProps = {
  imageSource?: string,
  key: string,
};

type Props = {
  items: Array<BannerItemProps>,
  theme: Object,
  push: Function,
};

const bannerHeight = Dimensions.get('window').width * 0.45;
const bannerWidth = Dimensions.get('window').width;
const bannerImageWidth = bannerWidth;

const BannerImage = styled(Image)`
  height: ${bannerHeight}px;
  width: ${bannerImageWidth}px;
  resize-mode: ${Image.resizeMode.stretch};
`;

class Banner extends Component {
  props: Props;

  state = { activeSlide: 0 };

  renderItem = ({ item }) => {
    const { push } = this.props;
    return (
      <Touchable
        onPress={() =>
          push({
            routeName: 'WebViewScreen',
            params: { uri: item.urlLink },
          })
        }
      >
        <View
          key={item.key}
          justifyContent="center"
          backgroundColor="transparent"
        >
          <BannerImage
            source={{
              uri: item.imageSource,
            }}
          />
        </View>
      </Touchable>
    );
  };

  pagination = () => {
    const { items, theme } = this.props;
    const { activeSlide } = this.state;
    return (
      <Pagination
        carouselRef={this.carouselRef}
        dotsLength={items.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          position: 'absolute',
          bottom: -20,
          width: bannerWidth,
        }}
        dotStyle={{
          backgroundColor: theme.color.primary,
          width: 8,
          height: 8,
          borderRadius: 4,
        }}
        inactiveDotStyle={{
          backgroundColor: theme.color.dark.text.secondary,
        }}
        tappableDots={!!this.carouselRef}
      />
    );
  };

  render() {
    const { items } = this.props;
    return (
      <View>
        <Carousel
          ref={c => {
            this.carouselRef = c;
          }}
          loop
          autoplayInterval={5000}
          autoplayDelay={5000}
          autoplay
          data={items}
          renderItem={this.renderItem}
          sliderWidth={bannerWidth}
          itemWidth={bannerImageWidth}
          onSnapToItem={index => this.setState({ activeSlide: index })}
          containerCustomStyle={{ height: bannerHeight }}
          lockScrollWhileSnapping
          enableSnap
        />
        {this.pagination()}
      </View>
    );
  }
}

Banner = withTheme(Banner);
Banner = connect(
  null,
  { push: StackActions.push },
)(Banner);

export default Banner;
