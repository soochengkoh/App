import React from 'react';
import styled, { css } from 'styled-components/native';
import Icon from './Icon';
import View from './View';
import Text from './Text';
import Touchable from './Touchable';

const LeftIcon = styled(Icon)`
  margin-right: 15px;
`;
const RightIcon = styled(Icon)`
  margin-left: 15px;
`;
const ListItemContainer = styled(View)`
  flex-direction: row;
  align-items: center;
  padding-vertical: 16px;
  padding-horizontal: 15px;
  background-color: white;
  ${({ hideBottomBorder, theme }) =>
    !hideBottomBorder &&
    css`
      border-bottom-width: 1px;
      border-bottom-color: ${theme.color.dark.divider};
    `};
`;
const ListItemLeft = styled(View)`
  flex-direction: row;
  flex: 1;
  align-items: center;
`;
const ListItemRight = styled(View)`
  flex-direction: row;
  align-items: center;
`;

type SuffixProps = {
  suffix?: any,
  suffixProps?: Object,
};

type LabelProps = {
  label?: any,
  labelProps?: Object,
};

type Props = {
  leftIcon?: string | boolean,
  rightIcon?: string | boolean,
  rightIconProps?: Object,
  leftIconProps?: Object,
  hideBottomBorder?: boolean,
  onPress?: Function,
  ...SuffixProps,
  ...LabelProps,
};

const Suffix = ({ suffix: S, suffixProps }: SuffixProps) => {
  if (!S) {
    return null;
  }
  if (typeof S === 'string') {
    return <Text {...suffixProps}>{S}</Text>;
  }
  return S;
};

const Label = ({ label: L, labelProps }: LabelProps) => {
  if (!L) {
    return null;
  }
  if (typeof L === 'string') {
    return (
      <Text flex={1} {...labelProps}>
        {L}
      </Text>
    );
  }
  return L;
};

const ListItem = ({
  leftIcon,
  label,
  labelProps,
  rightIcon,
  suffix,
  suffixProps,
  rightIconProps,
  leftIconProps,
  onPress,
  hideBottomBorder,
}: Props) => {
  const content = (
    <ListItemContainer hideBottomBorder={hideBottomBorder}>
      <ListItemLeft>
        {!!leftIcon && (
          <LeftIcon name={leftIcon} color="primary" {...leftIconProps} />
        )}
        <Label label={label} labelProps={labelProps} />
      </ListItemLeft>
      <ListItemRight>
        <Suffix suffix={suffix} suffixProps={suffixProps} />
        {!!rightIcon && <RightIcon name={rightIcon} {...rightIconProps} />}
      </ListItemRight>
    </ListItemContainer>
  );
  if (onPress) {
    return <Touchable onPress={onPress}>{content}</Touchable>;
  }
  return content;
};

ListItem.defaultProps = {
  label: '',
  suffix: '',
  hideBottomBorder: false,
  rightIconProps: {},
  leftIconProps: {},
};

export default ListItem;
