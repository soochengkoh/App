import React, { Component } from 'react';
import { InteractionManager, FlatList, Keyboard } from 'react-native';
import styled from 'styled-components/native';
import { withTheme } from 'styled-components';
import find from 'lodash/find';
import get from 'lodash/get';
import View from './View';
import Modal from './Modal';
import Touchable from './Touchable';
import Divider from './Divider';
import Header from './Header';
import Text from './Text';
import ListItem from './ListItem';
import SelectedItemIcon from './SelectedItemIcon';

const ITEM_HEIGHT = 46;
const ITEM_HEIGHT_WITH_DIVIDER = ITEM_HEIGHT + 1;

const getModalStyle = length => {
  let number;
  if (length >= 4) {
    number = 5.5;
  } else {
    number = length + 1.5;
  }
  return {
    height: number * ITEM_HEIGHT_WITH_DIVIDER,
  };
};

type Props = {
  title: string,
  onChange: Function,
  items: Array<any>,
  value?: any,
  label?: string,
  placeholder?: string,
  meta?: any,
  hidePaddingVertical?: boolean,
  hideBottomBorder?: boolean,
  disabled?: boolean,
  theme: Object,
  icon?: any,
  iconProps?: Object,
};

const StyledDivider = styled(Divider)`
  margin-horizontal: 10px;
`;

const SelectItemInner = styled(View)`
  padding-horizontal: 15px;
  height: ${ITEM_HEIGHT}px;
  flex-direction: row;
  align-items: center;
`;

const SelectItem = ({
  item,
  onChange,
  onCloseModal,
  selectedValue,
}: {
  onChange: Function,
  item: Object,
  selectedValue?: any,
  onCloseModal: Function,
}) => {
  const { value, label } = item;
  return (
    <View key={value}>
      <Touchable
        onPress={() => {
          onChange(value);
          onCloseModal();
        }}
      >
        <SelectItemInner>
          <Text flex={1}>{label}</Text>
          {selectedValue === value && <SelectedItemIcon />}
        </SelectItemInner>
      </Touchable>
      <StyledDivider />
    </View>
  );
};

class Select extends Component {
  props: Props;

  static defaultProps = {
    items: [],
  };

  state = {
    modalVisible: false,
  };

  scrollToSelectedItem = selectedItemIndex => {
    InteractionManager.runAfterInteractions(() => {
      if (this.flatList) {
        this.flatList.scrollToIndex({
          viewPosition: 0.5,
          index: selectedItemIndex,
        });
      }
    });
  };

  onCloseModal = () => {
    this.setState({ modalVisible: false });
  };

  onOpenModal = () => {
    this.setState({ modalVisible: true });
    Keyboard.dismiss();
  };

  getSelectedItem = () => {
    const { items, value } = this.props;
    return find(items, { value });
  };

  getSelectedItemIndex = () => {
    const { items } = this.props;
    const selectedItem = this.getSelectedItem();
    let selectedItemIndex = -1;
    if (selectedItem) {
      selectedItemIndex = items.indexOf(selectedItem);
    }
    return selectedItemIndex;
  };

  getSuffixColor = () => {
    const { theme } = this.props;
    const selectedItem = this.getSelectedItem();
    if (selectedItem) {
      return theme.color.dark.text.primary;
    }
    return theme.color.dark.text.hint;
  };

  getSuffix = () => {
    const { placeholder, title } = this.props;
    const selectedItem = this.getSelectedItem();
    return get(selectedItem, 'label') || placeholder || title;
  };

  onSelectButtonPress = () => {
    const selectedItemIndex = this.getSelectedItemIndex();
    this.onOpenModal();
    if (selectedItemIndex > -1) {
      this.scrollToSelectedItem(selectedItemIndex);
    }
  };

  render() {
    const {
      title,
      items,
      value,
      label,
      onChange,
      icon,
      iconProps,
      meta,
      // hideBottomBorder,
      disabled,
    } = this.props;
    const { touched, error } = meta || {};
    const modalStyle = getModalStyle(items.length);
    const selectedItemIndex = this.getSelectedItemIndex();
    return (
      <View>
        <Modal
          shouldAnimateOnRequestClose
          containerStyle={{
            flexDirection: 'row',
            alignItems: 'flex-end',
          }}
          style={{
            flex: 1,
            backgroundColor: 'white',
            borderTopLeftRadius: 15,
            borderTopRightRadius: 15,
            ...modalStyle,
          }}
          onClose={this.onCloseModal}
          visible={this.state.modalVisible}
        >
          <View style={{ flex: 1 }}>
            <Header
              title={title}
              backgroundColor="white"
              color="dark.text.primary"
              headerContainerProps={{
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15,
              }}
            />
            <FlatList
              ref={flatList => {
                this.flatList = flatList;
              }}
              keyboardShouldPersistTaps="handled"
              data={items}
              renderItem={({ item }) => (
                <SelectItem
                  item={item}
                  onChange={onChange}
                  onCloseModal={this.onCloseModal}
                  selectedValue={value}
                />
              )}
              getItemLayout={(data, index) => ({
                length: ITEM_HEIGHT_WITH_DIVIDER,
                offset: ITEM_HEIGHT_WITH_DIVIDER * index,
                index,
              })}
              keyExtractor={item => item.value}
            />
          </View>
        </Modal>
        <Touchable
          onPress={() => {
            this.onOpenModal();
            if (selectedItemIndex > -1) {
              this.scrollToSelectedItem(selectedItemIndex);
            }
          }}
        >
          <ListItem
            label={label}
            suffix={this.getSuffix()}
            suffixProps={{
              color: this.getSuffixColor(),
            }}
            leftIcon={icon}
            leftIconProps={iconProps}
            rightIcon="arrow_down"
            rightIconProps={{
              color: 'dark.text.secondary',
            }}
            onPress={disabled ? undefined : this.onSelectButtonPress}
          />
        </Touchable>
      </View>
    );
  }
}

export default withTheme(Select);
