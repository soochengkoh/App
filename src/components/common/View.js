import styled from 'styled-components/native';
import { isStyledBy, is, isStyledThemeBy } from '../../utils/styles';

const StyledView = styled.View`
  ${isStyledBy('size', {
    cssProp: 'height',
  })};
  ${isStyledBy('size', {
    cssProp: 'width',
  })};
  ${is('center')`
    justify-content: center;
    align-items: center;
  `};
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
    defaultValue: 'transparent',
  })};
  ${isStyledThemeBy('borderColor', {
    at: 'color',
  })};
`;

export default StyledView;
