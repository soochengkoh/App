import React from 'react';
import Icon from './Icon';

export default () => <Icon name="tick" size="25" color="primary" />;
