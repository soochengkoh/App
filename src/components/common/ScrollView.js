import React from 'react';
import styled from 'styled-components/native';

const StyledScrollView = styled.ScrollView.attrs({
  contentContainerStyle: props =>
    (props.fullscreen
      ? {
        paddingBottom: props.theme.footerPaddingBottom,
      }
      : undefined),
})``;

type Props = {
  fullscreen?: boolean,
};

const ScrollView = (props: Props) => <StyledScrollView {...props} />;

ScrollView.defaultProps = {
  fullscreen: true,
};

export default ScrollView;
