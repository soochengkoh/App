import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

type Props = {
  onSubmit: Function,
};

class Form extends Component {
  props: Props;

  constructor(props, context) {
    super(props, context);
    if (!context._reduxForm) {
      throw new Error('Form must be inside a component decorated with reduxForm()');
    }
  }

  componentWillMount() {
    this.context._reduxForm.registerInnerOnSubmit(this.props.onSubmit);
  }

  render() {
    return <View {...this.props} />;
  }
}

Form.contextTypes = {
  _reduxForm: PropTypes.object,
};

export default Form;
