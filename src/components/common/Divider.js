import React from 'react';
import styled from 'styled-components/native';
import { is } from '../../utils/styles';

type Props = {
  large?: boolean,
  tlarge?: boolean,
};

const View = styled.View`
  border-top-color: ${({ theme }) => theme.color.dark.divider};
  border-top-width: 1px;
  ${is('large')`
    border-top-width: 8px;
    `};
  ${is('light')`
    border-top-color: ${({ theme }) => theme.color.light.divider};
      `};
`;

const Divider = (props: Props) => <View {...props} />;

export default Divider;
