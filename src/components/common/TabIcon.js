import React from 'react';
import Icon from './Icon';

type Props = {
  tintColor?: string,
};

const TabIcon = (props: Props) => (
  <Icon size={20} color={props.tintColor} {...props} />
);

export default TabIcon;
