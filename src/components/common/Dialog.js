// @flow

import React, { Component } from 'react';
import styled from 'styled-components/native';
import View from './View';
import Button from './Button';
import Header from './Header';
import Modal from './Modal';
import Text from './Text';

const ButtonList = ({ actions }: { actions?: Array<Object> }) => {
  if (!actions) {
    return null;
  }
  return (
    <View flexDirection="row" justifyContent="space-between" padding={5}>
      {actions.map(action => (
        <Button
          key={action.label}
          borderRounded
          flex={1}
          margin="0 5px 5px 5px"
          {...action}
        >
          {action.label}
        </Button>
      ))}
    </View>
  );
};

type Props = {
  title?: string,
  visible: boolean,
  onClose?: Function,
  children?: any,
  actions?: Array<Object>,
  dialogContainerProps?: Object,
};

const DialogContainer = styled.View`
  background-color: white;
  border-radius: 25;
  width: 300;
  overflow: hidden;
`;

class Dialog extends Component {
  props: Props;

  render() {
    const {
      title,
      visible,
      onClose,
      children,
      actions,
      dialogContainerProps,
    } = this.props;

    return (
      <Modal visible={visible} onClose={onClose}>
        <DialogContainer dialogContainerProps={dialogContainerProps}>
          {!!title && (
            <Header
              title={title}
              headerContainerProps={{
                borderTopLeftRadius: 25,
                borderTopRightRadius: 25,
              }}
            />
          )}
          {children}
          <ButtonList actions={actions} />
        </DialogContainer>
      </Modal>
    );
  }
}

export default Dialog;
