import React from 'react';
import styled, { css } from 'styled-components/native';
import { withTheme } from 'styled-components';
import isString from 'lodash/isString';
import Touchable from './Touchable';
import Text from './Text';
import { is, isStyledBy, isStyledThemeBy } from '../../utils/styles';

type Props = {
  footer?: boolean,
  block?: boolean,
  borderRounded?: boolean,
  disabled?: boolean,
  onPress: Function,
  color?: string,
  children: any,
  theme: Object,
};

const Button = ({
  children,
  disabled,
  color,
  onPress,
  theme,
  ...restProps
}: Props) => {
  let content = null;
  if (isString(children)) {
    content = (
      <Text color={disabled ? theme.color.disabledTextColor : color}>
        {children}
      </Text>
    );
  } else {
    content = children;
  }
  return (
    <Touchable
      onPress={() => !disabled && onPress && onPress()}
      disabled={disabled}
      {...restProps}
    >
      {content}
    </Touchable>
  );
};

const StyledButton = styled(Button).attrs({
  paddingHorizontal: props =>
    props.padding || props.paddingHorizontal || '16px',
  paddingVertical: props => props.padding || props.paddingVertical || '12px',
  color: ({
    color, primary, secondary, disabled, theme, flat,
  }) => {
    if (disabled) {
      return theme.color.disabledTextColor;
    }
    if (color) {
      return color;
    }
    if (primary) {
      return flat ? theme.color.primary : theme.color.light.text.primary;
    }
    if (secondary) {
      return flat ? theme.color.secondary : theme.color.light.text.primary;
    }
  },
})`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  align-self: flex-start;
  ${isStyledBy('paddingHorizontal')};
  ${isStyledBy('paddingVertical')};
  ${isStyledBy('margin')};
  ${isStyledBy('flex')};
  ${isStyledBy('width')};
  ${isStyledThemeBy('backgroundColor', {
    cssProp: 'background-color',
    at: 'color',
    defaultValue: props => props.theme.color.white,
  })};
  ${({ footer, theme }) =>
    footer &&
    css`
      height: ${theme.footerHeight};
      width: 100%;
      padding-bottom: ${theme.footerPaddingBottom};
    `};
  ${is('borderRounded')`
      border-radius: 25px;
    `};
  ${is('border')`
      border-color: ${props => props.theme.color.dark.text.hint};
      border-width: 1px;
    `};
  ${is('block')`
      width: 100%;
    `};
  ${is('primary')`
      background-color: ${props => props.theme.color.primary};
    `};
  ${is('secondary')`
      background-color: ${props => props.theme.color.secondary};
    `};
  ${is('disabled')`
      background-color: ${props => props.theme.color.disabledBackgroundColor};
    `};
  ${is('flat')`
      background-color: white;
    `};
`;

export default withTheme(StyledButton);
