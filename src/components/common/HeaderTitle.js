import React, { Component } from 'react';
import styled from 'styled-components/native';
import Text from './Text';
import { isStyledThemeBy } from '../../utils/styles';

type Props = {
  title: string,
  color?: string,
};

const HeaderTitleContainer = styled.View`
  position: absolute;
  bottom: 0;
  left: ${props => props.theme.appBarHeight};
  right: ${props => props.theme.appBarHeight};
  top: 0;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const HeaderText = styled(Text)`
  font-size: ${props => props.theme.headerTitleStyle.fontSize};
  ${isStyledThemeBy('color', {
    at: 'color',
    defaultValue: props => props.theme.headerTitleStyle.color,
  })};
`;

class HeaderTitle extends Component {
  props: Props;

  render() {
    const { title, color } = this.props;
    return (
      <HeaderTitleContainer>
        <HeaderText numberOfLines={1} color={color}>
          {title}
        </HeaderText>
      </HeaderTitleContainer>
    );
  }
}

export default HeaderTitle;
