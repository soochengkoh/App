import React, { Component } from 'react';
import styled from 'styled-components/native';
import HeaderTitle from './HeaderTitle';
import View from './View';
import { isStyledThemeBy } from '../../utils/styles';

type Props = {
  title: string,
  leftComponent?: any,
  rightComponent?: any,
  color?: string,
  backgroundColor?: string,
  headerContainerProps?: Object,
};

const SideComponentContainer = styled(View)`
  bottom: 0;
  position: absolute;
  justify-content: center;
  top: 0;
`;

const LeftComponentContainer = styled(SideComponentContainer)`
  left: 5;
`;

const RightComponentContainer = styled(SideComponentContainer)`
  right: 5;
`;

const HeaderContainer = styled(View)`
  ${isStyledThemeBy('backgroundColor', {
    at: 'color',
    defaultValue: props => props.theme.color.primary,
  })};
  border-bottom-color: ${props => props.theme.color.dark.divider};
  border-bottom-width: 1px;
  elevation: 2;
  height: ${props => props.theme.appBarHeight};
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  overflow: hidden;
`;

class Header extends Component {
  props: Props;

  static defaultProps = {
    leftComponent: View,
    rightComponent: View,
  };

  render() {
    const {
      title,
      color,
      backgroundColor,
      leftComponent: LC,
      rightComponent: RC,
      headerContainerProps,
    } = this.props;
    return (
      <HeaderContainer
        backgroundColor={backgroundColor}
        {...headerContainerProps}
      >
        <LeftComponentContainer>
          <LC />
        </LeftComponentContainer>
        <HeaderTitle title={title} color={color} />
        <RightComponentContainer>
          <RC />
        </RightComponentContainer>
      </HeaderContainer>
    );
  }
}

export default Header;
