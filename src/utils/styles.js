import { css } from 'styled-components/native';
import get from 'lodash/get';
import isFunction from 'lodash/isFunction';

const resolveDefaultValue = (props, defaultValue) => {
  if (isFunction(defaultValue)) {
    return defaultValue(props);
  }
  return defaultValue;
};

const styledIf = (method, condition) => (...names) => (
  styledFn,
  ...rest
) => props =>
  names[method](name => Boolean(props[name]) === condition) &&
  css(styledFn, ...rest);

const is = styledIf('every', true);
const isNot = styledIf('every', false);
const isOr = styledIf('some', true);
const isSomeNot = styledIf('some', false);

const styledBy = (prop, { cssProp, defaultValue } = {}) => props =>
  `${cssProp || prop}: ${props[prop] ||
    resolveDefaultValue(props, defaultValue)}`;

const isStyledBy = (prop, { cssProp, defaultValue } = {}) => props =>
  (!!props[prop] || defaultValue !== undefined) &&
  css`
    ${styledBy(prop, {
    cssProp,
    defaultValue: resolveDefaultValue(props, defaultValue),
  })};
  `;

const isStyledThemeBy = (
  prop,
  { cssProp = prop, at, defaultValue } = {},
) => props => {
  const themeAt = at ? `theme.${at}.${props[prop]}` : `theme.${props[prop]}`;
  return get(props, themeAt)
    ? css`
        ${cssProp}: ${get(props, themeAt)};
      `
    : isStyledBy(prop, {
      cssProp,
      defaultValue: resolveDefaultValue(props, defaultValue),
    });
};

export { is, isNot, isOr, isSomeNot, styledBy, isStyledBy, isStyledThemeBy };
