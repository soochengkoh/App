import Toast from 'react-native-root-toast';

export const { durations, positions } = Toast;

const defaultOptions = {
  position: positions.CENTER,
  duration: durations.SHORT,
  shadow: false,
  animation: true,
  hideOnPress: true,
  delay: 0,
};

export default {
  show: (message, options = {}) => {
    Toast.show(message, {
      ...defaultOptions,
      ...options,
    });
  },
};
