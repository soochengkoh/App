import axios from 'axios';
import queryString from 'query-string';
import forOwn from 'lodash/forOwn';
import isObject from 'lodash/isObject';
import config from '../config';
import toast from './toast';

let store;

const jsonToFormEncoded = data => {
  const str = [];
  forOwn(data, (dataValue, dataKey) => {
    if (isObject(dataValue)) {
      const d = dataValue;
      forOwn(d, (value, key) => {
        if (value) {
          str.push(`${dataKey}[${encodeURIComponent(key)}]=${encodeURIComponent(value)}`);
        }
      });
    } else if (dataValue) {
      str.push(`${encodeURIComponent(dataKey)}=${encodeURIComponent(dataValue)}`);
    }
  });
  return str.join('&');
};

const paramsSerializer = params => queryString.stringify(params);

const formUrlEncodedInterceptor = config => {
  if (config.headers['Content-Type'] === 'application/x-www-form-urlencoded') {
    config.data = jsonToFormEncoded(config.data);
  }
  return config;
};

const addInterceptors = api => {
  api.interceptors.request.use(formUrlEncodedInterceptor);
  api.interceptors.response.use(undefined, error => {
    if (error.message === 'Network Error') {
      toast.show('Network Error, Please try again');
    }
    return Promise.reject(error);
  });
  return api;
};

const api = addInterceptors(axios.create({
  baseURL: config.DOMAIN,
  paramsSerializer,
  withCredentials: true,
}));

export const configureApi = _store => {
  store = _store;
};

export default api;
