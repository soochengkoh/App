import { combineEpics, ofType } from 'redux-observable';
import { map } from 'rxjs/operators';
import example from '../screens/example';

const pingEpic = action$ =>
  action$.pipe(
    ofType({ type: 'PING' }),
    map(() => ({
      type: 'PONG',
    })),
  );

export default combineEpics(pingEpic, example.epic);
