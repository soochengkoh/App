import { applyMiddleware, createStore, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { AsyncStorage } from 'react-native';
import { persistStore, persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import rootReducer from './ducks';
import rootEpic from './epics';
import { configureApi } from './utils/api';
import { NAME as LOCALSTORAGE_NAME } from './containers/localstorage/ducks';

/* global __DEV__ */
if (!__DEV__) {
  // remove console in production
  // eslint-disable-next-line  no-global-assign
  console = {};
  console.log = () => {};
  console.error = () => {};
  console.warn = () => {};
}

const epicMiddleware = createEpicMiddleware();
let middleware = [epicMiddleware];

/* global __DEV__ */
if (__DEV__) {
  const { createLogger } = require('redux-logger');
  middleware = [...middleware, createLogger()];
}

const config = {
  key: 'app',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
  whitelist: [LOCALSTORAGE_NAME],
  // debug: true,
};

export default function configureStore() {
  const reducers = persistReducer(config, rootReducer);
  const enhancer = compose(applyMiddleware(...middleware));
  const store = createStore(reducers, {}, enhancer);

  epicMiddleware.run(rootEpic);

  configureApi(store);

  if (module.hot) {
    // hot reload epics
    module.hot.accept('./epics', () => {
      const nextRootEpic = require('./epics').default;

      epicMiddleware.replaceEpic(nextRootEpic);
    });

    // hot reload reducers
    module.hot.accept(() => {
      const nextRootReducer = require('./ducks').default;

      store.replaceReducer(nextRootReducer);
    });
  }

  const persistor = persistStore(store);
  // persistor.purge();

  return { persistor, store };
}
