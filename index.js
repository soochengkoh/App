import { AppRegistry } from 'react-native';
import './ignore-warning';
import App from './src/App';

AppRegistry.registerComponent('App', () => App);
